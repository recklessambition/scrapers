import urllib2, StringIO, gzip, zlib, requests, json,time,os
import xml.etree.ElementTree as ET
from random import randint
import subprocess
import StringIO
import traceback
from urllib2 import quote
import hashlib

# See bottom for what to edit

def target():
    ''' Top level function for requesting hunting targets '''
    response = [None]
    responseDict = None

    if(make_request(response,'getHuntingTargets','')):

        return response[0]

def make_request(response,message,body):
    randsleep(1,10)
    response[0] = None
    global player_id,pass_timestamp,kong_id,timestamp
    timestamp = str(int(time.time()))
    try:
        url = "http://mobile.tyrantonline.com/api.php?message="+message+"&user_id="+player_id+"&timestamp="+timestamp

        req = {"User-Agent":"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:35.0) Gecko/20100101 Firefox/35.0",
        "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
        "Accept-Language":"en-US,en;q=0.5",
        "Accept-Encoding":"gzip, deflate",
        "Connection":"keep-alive",
        "Content-Type":"application/x-www-form-urlencoded",
        "X-Unity-Version":"4.6.3f1"}
        seed = '@B3%Y6'
        m=hashlib.md5(seed+str(player_id)+str(timestamp)).hexdigest()
        body = pass_timestamp+"&timestamp="+timestamp+"&hash="+str(m)+"&message="+message+"&client_version=45&device_type=Intel(R)+Core(TM)+i5-4690K+CPU+%40+3.50GHz+(16328+MB)&os_version=Windows+7+Service+Pack+1+(6.1.7601)+64bit&platform=Web&"+kong_id+body

        response[0] = requests.post(url=url,data=body,headers=req)
        ans = json.loads(response[0].text, encoding=response[0].headers['content-encoding'])
        response[0].close()
        response[0] = ans
    except:
        print("hwat")
        return False

    return True

def make_fast_request(response,message,body):
    randsleep(1,4)
    response[0] = None
    global player_id,pass_timestamp,kong_id,timestamp
    timestamp = str(int(time.time()))
    try:
        url = "http://mobile.tyrantonline.com/api.php?message="+message+"&user_id="+player_id+"&timestamp="+timestamp

        req = {"User-Agent":"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:35.0) Gecko/20100101 Firefox/35.0",
        "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
        "Accept-Language":"en-US,en;q=0.5",
        "Accept-Encoding":"gzip, deflate",
        "Connection":"keep-alive",
        "Content-Type":"application/x-www-form-urlencoded",
        "X-Unity-Version":"4.6.3f1"}
        seed = '@B3%Y6'
        m=hashlib.md5(seed+str(player_id)+str(timestamp)).hexdigest()
        body = pass_timestamp+"&timestamp="+timestamp+"&hash="+str(m)+"&message="+message+"&client_version=45&device_type=Intel(R)+Core(TM)+i5-4690K+CPU+%40+3.50GHz+(16328+MB)&os_version=Windows+7+Service+Pack+1+(6.1.7601)+64bit&platform=Web&"+kong_id+body

        response[0] = requests.post(url=url,data=body,headers=req)
        ans = json.loads(response[0].text, encoding=response[0].headers['content-encoding'])
        response[0].close()
        response[0] = ans
    except:
        print("hwat")
        return False

    return True

def refresh_targets(response):
    ''' Refresh targets '''
    global fight_count
    if (int(response['user_data']['money'])>int(response['gold_rivals_cost'])) and (fight_count > 40):
        print("Purchased refresh")
        response = [None]
        make_request(response,'buyRivalsRefresh','')
        time.sleep(1)
        fight_count = 0
        return True
    return False

def choose_fight(response,flag,brflag):
    ''' Function to choose which player to battle.

        Flag = 0 -> fight lowest BR player (will refresh when lowest BR player is above your BR)
        Flag = 1 -> fight players around your BR (will refresh when nobody is around your BR (+/- your BR/3) )
        Flag = 2 -> fight top BR player (will refresh when highest BR player below your BR)

        Note also that brflag will override refresh to occur every [brflag] fights'''
    if int(response['user_data']['stamina']) == 0:
        return False # stop fighting

    if brflag != -1:
        global fight_count
        if fight_count > brflag:
            ans = refresh_targets(response)
    
    targets = response['hunting_targets']
    enemy_id = ''
    low = -1
    low_id = ''
    high = 0
    high_id = ''
    mid_id = -1
    mid_br = 0
    your_br = int(response['user_data']['hunting_elo'])
    margin = your_br//3
    br_high = your_br + margin
    br_low = your_br - margin
    keys = targets.keys()
    for key in keys:
        i = targets[key]
        br = int(i['hunting_elo'])
        uid = int(i['user_id'])
        if (low==-1) or (low > br):
            low = br
            low_id = uid
        if high < br:
            high = br
            high_id = uid
        if (br>low) and (br<high):
            mid_id = uid
            mid_br = br

    if flag == 2:
        if high < your_br:
            ans = refresh_targets(response)
            if ans:
                return True
        print("Fighting user ID: "+str(high_id)+" with BR: "+str(high))
        fight(response,high_id)
    elif flag == 1:
        if mid_br == -1:
            ans = refresh_targets(response)
            print("Middle override, Fighting user ID: "+str(low_id)+" with BR: "+str(low))
            fight(response,low_id)
            return True
        print("Fighting user ID: "+str(mid_id)+" with BR: "+str(mid_br))
        fight(response,mid_id)
    else:
        if low > your_br:
            ans = refresh_targets(response)
            if ans:
                return True
        print("Fighting user ID: "+str(low_id)+" with BR: "+str(low))
        fight(response,low_id)

    return True

global wins, losses,gold,brgain,fight_count
wins = 0
losses = 0
gold = 0
brgain = 0
fight_count = 0

def fight(targets,user):
    ''' Fights user using info in dict targets '''
    global wins, losses,gold,brgain,fight_count
    global player_id,pass_timestamp,kong_id
    response = [None]
    responseDict = None
    time.sleep(1)
    if(make_request(response,'startHuntingBattle','&target_user_id='+str(user))):
        # Check auto flag
        response = response[0]
        try:
            battle_id = response['battle_data']['battle_id']
        except:
            print("Target was invalid")
            raise ValueError
            return
        if response['user_data']['flags']['autopilot'] != u'1':
            make_request([0],'setUserFlag','&value=1&flag=autopilot')
        upkept = True
        while upkept:
            response = [None]
            if(make_request(response,'playCard','&battle_id='+str(battle_id)+'&skip=1&host_id='+player_id)): # Skip after auto
                response = response[0]
                #print(response)
                upkept = response['battle_data']['upkept']
            else:
                print("hwat")
                return
        win = bool(int(response['battle_data']['winner']))
        if win:
            print("Won battle")
            wins += win
        else:
            print("Lost battle")
            losses += not win
        gold += int(response['battle_data']['rewards'][0]['gold'])
        brgain += int(response['battle_data']['rewards'][0]['rating_change'])
        fight_count+=1

def mission(mission_id):
    print("Mission: "+str(mission_id))
    response = [None]
    make_request(response,"startMission","&mission_id="+str(mission_id))
    response = response[0]
    battle_id = response['battle_data']['battle_id']
    randsleep(1,3)
    global order_flag,order_deck
    if order_flag:
        if response['user_data']['flags']['autopilot'] != u'0':
            make_request([0],'setUserFlag','&value=0&flag=autopilot')
            print("Set auto flag to 0")
            randsleep(1,3)
        deck = response['battle_data']['attack_deck']
        index = 4
        hand = [(deck['1'],1),(deck['2'],2),(deck['3'],3)]
        while bool(response['battle_data']['upkept']):
            move = -1
            for i in range(len(order_deck)):
                for j in range(len(hand)):
                    if (int(hand[j][0])>=int(order_deck[i][0])) and (int(hand[j][0])<=(int(order_deck[i][0])+5)):
                        print("Hand: "+str(hand))
                        move = hand.pop(j)
                        move = move[1]
                        print("Playing: "+order_deck[i][1])
                        
                        if (index<=len(deck)):
                            hand.append((deck[str(index)],index))
                            index+=1
                        break
                if move!=-1:
                    break
            if move==-1:
                for k in range(i,len(order_deck)):
                    move = hand.pop(j)
                    a = move[0]
                    move = move[1]
                    print("Deck goof")
                    print("Playing card: "+str(a))
                    response = [None]
                    make_fast_request(response,"playCard","&battle_id="+str(battle_id)+"&skip=0&card_uid="+str(move)+"&host_id="+player_id)
                    response = response[0]
                win = bool(int(response['battle_data']['winner']))
                if win:
                    print("Won mission. Fix your deck.")
                else:
                    print("Lost mission. Fix your deck.")
                return
            response = [None]
            make_fast_request(response,"playCard","&battle_id="+str(battle_id)+"&skip=0&card_uid="+str(move)+"&host_id="+player_id)
            response = response[0]
        win = bool(int(response['battle_data']['winner']))
        if win:
            print("Won mission")
        else:
            print("Lost mission")
    else:
        if response['user_data']['flags']['autopilot'] != u'1':
            make_request([0],'setUserFlag','&value=1&flag=autopilot')
            print("Set auto flag to 1")
            randsleep(1,3)
        response = [None]
        make_request(response,"playCard","&battle_id="+str(battle_id)+"&skip=1&host_id="+player_id)
        response = response[0]
        
        win = bool(int(response['battle_data']['winner']))
        if win:
            print("Won mission")
        else:
            print("Lost mission")

def randsleep(start,stop):
    a = randint(start,stop)
    b = ''
    if a>30:
        b = ' ('+str(float(a)/(60*5.0))+' attacks)'
    print("Sleeping for "+str(a)+" seconds."+b)
    time.sleep(a)

def climb():
    global climb_mission, fifty_mission, start_mission, end_mission, default_mission,current_mission
    global eventflag
    rwar = True
    while rwar:
        response = [None]

        make_request(response,'update','')
        response = response[0]
        if climb_mission == 1:
            # Go from start_mission until end_mission, getting level 10 on each
            # Once end_mission is completed, repeat default_mission
            try:
                if (int(response['mission_completions'][str(current_mission)]['star3'])==1):
                    if (current_mission == end_mission):
                        print("Mission climbing completed. Reverting to default mission.")
                        if eventflag:
                            eventflag = 0
                            print("Reset default mission")
                        climb_mission = 0
                        continue
                    else:
                        print("Mission completed. Continuing to next mission.")
                        current_mission += 1
                        continue
            except:
                pass
            rwar = bool(int(response['user_data']['energy']) >= mission_energy(current_mission))
            if rwar:
                mission(current_mission)
            else:
                print("Climb: Energy exhausted.")
        elif climb_mission == 2:
            # Go from start_mission until end_mission, getting level 1 on each
            # Once end_mission is completed, repeat default_mission
            # If lost once, go to default
            try:
                if (bool(response['mission_completions'][str(current_mission)]['complete'])):
                    if (current_mission == end_mission):
                        if eventflag:
                            print("Finished speeding event. Proceeding to 50/50 boss")
                            climb_mission = 3
                            continue
                        else:
                            print("Mission speeding completed. Reverting to default mission.")
                            climb_mission = 0
                            continue
                    else:
                        print("Mission sped. Continuing to next mission.")
                        current_mission += 1
                        continue
            except:
                pass
            rwar = bool(int(response['user_data']['energy']) >= mission_energy(current_mission))
            if rwar:
                mission(current_mission)
            else:
                print("Speed: Energy exhausted.")
        elif climb_mission == 3:
            # Complete default_mission 50 times
            try:
                if (bool(response['mission_completions'][str(fifty_mission)]['complete'])):
                    number = int(response['mission_completions'][str(fifty_mission)]['number'])
                    if (number >= 50):
                        if eventflag:
                            print("Finished 50/50-ing boss. Proceeding to max other levels")
                            current_mission = start_mission
                            climb_mission = 1
                            continue
                        else:
                            print("Mission 50/50-ing completed. Maintaining current mission.")
                            climb_mission = 0
                            continue
            except:
                pass
            rwar = bool(int(response['user_data']['energy']) >= mission_energy(fifty_mission))
            if rwar:
                mission(fifty_mission)
            else:
                print("50/50: Energy exhausted.")
        elif climb_mission == 5:
            global cenergy
            rwar = bool(int(response['user_data']['energy']) >= int(cenergy))
            if rwar:
                global listy_final, coverride,listy_vip
                campaign(override=coverride,listy_final=listy_final,listy_vip=listy_vip)
            else:
                print("Campaign: Energy exhausted.")
        else:
            # Only do default_mission
            rwar = bool(int(response['user_data']['energy']) >= mission_energy(default_mission))
            if rwar:
                mission(default_mission)
            else:
                print("Static: Energy exhausted.\n")

def inv_scrape():
    global player_id
    inventory = player_id+" tempinv.txt"
    outf = open(inventory,'w')
    response = [None]
    make_request(response,'update','')
    response = response[0]
    try:
        cards = response['user_cards']
    except:
        print("Unable to parse cards?")
        outf.close()
        return
    keys = cards.keys()
    for i in range(len(cards)):
        num = int(cards[keys[i]]['num_owned'])
        if num:
            string = str('[')+str(keys[i])+str(']')
            if num>1:
                string += ' ('+str(num)+')'
            string += '\n'
            outf.write(string)
    outf.close()
    print("Completed inventory scrape")
    return inventory

def cli(inst,seek):
    f = StringIO.StringIO()
    process = subprocess.Popen(inst, stdout=subprocess.PIPE)
    flag = 0
    for line in iter(process.stdout.readline, ''):
        #sys.stdout.write(line)
        if line.find(seek)!=-1: #'Optimized Deck:'
            flag = 1
        if flag:
            f.write(line)
            print(line)
    return f

def merge(to_take, new, current_level):
    setty = set(new)
    for element in setty:
        num = new.count(element)
        newdex = 0
        counting = 0
        for k in range(len(to_take)):
            if to_take[k][0]==element:
                counting+=to_take[k][2]
        for i in range(num):
            if (counting < num):
                if counting==0:
                    to_take.append([element,current_level,num]) # card,level,frequency
                    counting+=num
                else:
                    for k in range(len(to_take)):
                        if to_take[k][0]==element:
                            to_take[k][2]=num
                            break
                break
    return to_take

def findnum(card):
    try:
        cwd = os.getcwd()+'\\data\\'
        tree = ET.parse(cwd+'cards.xml')
        root = tree.getroot()
    except:
        print("Please put this script in the TUO main folder")
        raise ValueError
    for child in root.findall('unit'):
        xmlcard = str(child.find('name').text)
        actualcard = card.split('-')
        if xmlcard == str(actualcard[0]):
            try:
                retcard = int(child.find('id').text)-1+int(actualcard[1])
            except:
                retcard = int(child.find('id').text)-1+6
            return retcard
    print("Could not find card with name: "+str(card))
    raise ValueError

def findcard(card):
    try:
        cwd = os.getcwd()+'\\data\\'
        tree = ET.parse(cwd+'cards.xml')
        root = tree.getroot()
    except:
        print("Please put this script in the TUO main folder")
        raise ValueError
    upgrades = {'1':3,'2':4,'3':6,'4':6,'5':6,}
    for child in root.findall('unit'):
        rarity = child.find('rarity').text
        upgrade = upgrades[rarity]
        if (int(card) >= int(child.find('id').text)) and (int(card) <= (int(child.find('id').text)+upgrade-1)):
            if int(card) == (int(child.find('id').text)+upgrade-1):
                return str(child.find('name').text)
            else:
                level = '-'+str(int(card) - int(child.find('id').text) + 1)
                return str(child.find('name').text) + level
    print("Couldn't find card with id: "+str(card))
    raise ValueError

def write_inventory(commander, listy):
    global player_id
    # Create temp inventory
    temp=str(player_id)+' campaign inventory.txt'
    tempinv = open(temp,'w')
    setty = set(listy)
    if commander:
        tempinv.write('['+str(findnum(commander))+']\n')
    for element in setty:
        numstring=''
        if listy.count(element)!=1:
            numstring=' ('+str(listy.count(element))+')'
        tempinv.write('['+str(findnum(element))+']'+numstring+'\n')
    tempinv.close() # Note, each filewrite overwrites file
    return temp

global campaign_flag
campaign_flag = 0

##def campaign():
##    if not campaign_flag:
##        init_campaign()
##    run_campaign()

def make_string(string):
    string = str(string).replace(" ","").replace("'",'"')
    return quote(string)

def campaign(override=0,listy_final=None,listy_vip=None):
    # Energy already checked
    global decks #Comma separated of the entire deck, commander included. Note commander will be fixed
    global campaign_flag
    global cname # Campaign name
    global clevel # Campaign difficulty (1-3)
    global inv_name # Name of inventory file
    
    a = decks.split(', ',1)
    commander = a[0]
    deck = a[1]
    iterations = 10000 # Change if necessary

    if not override:
        cards = []

        for i in range(1,8): # Assuming 7 levels to the campaign
            if i == 7:
                campaign = 'gw-defense' # Because don't care about card deaths
            else:
                campaign = 'campaign'
            enemy = cname+str(clevel)+'0'+str(i) #+'-'+str(clevel)
            command = 'tuo "'+decks+'" "'+enemy+'" '+campaign+' ordered -o="'+inv_name+'" -c climb '+str(iterations)
            print("Command: "+command)
            a = cli(command,'Optimized Deck:')
            a.seek(-1)
            b = a.read().strip().split(': ')
            a.close()
            print("First round sim. Level: "+str(i)+" Cards: "+b[2]+"\n\n")
            cards.append(b[2].split(', ',1)[1]) # cards

        print("Finished first round of sims. Cards currently:")
        print(cards) # Should have one element per level

        newcards = []

        for i in range(len(cards)):
            ncards = cards[i].split(', ')
            wcards = []
            for element in ncards:
                element = element.split(' #') # Duplicates from TUO handled here
                try:
                    for j in range(int(element[1])):
                        wcards.append(element[0])
                except:
                    wcards.append(element[0])
            newcards.append(', '.join(wcards))

        cards = newcards # Now duplicates just appear twice (e.g.  Baughe #2 as Baughe, Baughe)

        print("Now cards are: ")
        print(cards)
        
        # Now, all cards are simmed on climb throughout the missions, begin to filter through
        to_take = []
        scrap = []
        vip = []
        for j in range(len(cards)):
            current_level = len(cards)-1-j
            element = cards[current_level] # Goes in reverse, from level 7 to 1 (indices 6 to 0)
            new = []
            for i in element.split(', '):
                if '-' in i:
                    if i in scrap:
                        continue
                    else:
                        scrap.append(i)
                else:
                    new.append(i) #[i,j+1]
            to_take = merge(to_take, new, current_level)
            print("To take: "+str(to_take))
        # Now to_take has unknown number of cards, duplicates handled, various in scrap
        final_list = []
        print("Finished merging")
        print("To take: "+str(to_take))
        print("Scrap: "+str(scrap))
        expanded = []
        for element in to_take:
            for rwar in range(element[2]):
                expanded.append(element[0])
        print("Expanded: "+str(expanded))
        if len(expanded)<15: # Sort through scrap
            print("Less than 15 cards")
            final_list.extend(expanded)
            if (len(scrap)+len(expanded))<=15:
                final_list.extend(scrap)
            else:
                num = 15-len(expanded)
                for i in range(num):
                    final_list.append(scrap[i])
        elif len(expanded)>15: # Gotta cut down
            print("More than 15 cards")
            for i in range(15):
                final_list.append(expanded[i]) # Takes the first 15 cards
        else:
            print("Enough cards")
            for element in expanded:
                final_list = expanded[:]
        for i in range(4):
            vip.append(final_list[i]) # Sets first 4 cards to VIP status

    else:
        final_list = listy_final
        vip = listy_vip

    print("VIPs set as: "+str(vip))
    vipstr = ', '.join(vip) # for TUO
    print("Final list: "+str(final_list))

    temp = write_inventory(commander, final_list) # [Numbers] written, not names
    
    final_set = set(final_list)
    death_dict = {}
    send_dict = {}
    res_dict = {}
    count = 0
    for element in final_set:
        if (count + final_list.count(element))<=10:
            send_dict[str(findnum(element))]=str(final_list.count(element))
            count += final_list.count(element)
        else:
            res_dict[str(findnum(element))]=str(final_list.count(element))
        death_dict[str(findnum(element))]=0
            
    deck_send = make_string(str(send_dict)) # Deck
    res_send = make_string(str(res_dict)) #Reserves
    current = str(commander)+', '+', '.join(final_list)

    safeguard1 = 0
    safeguard2 = 0
    for element in send_dict.values():
        safeguard1 += int(element)
    for element in res_dict.values():
        safeguard2 += int(element)
    if (safeguard1>10) or (safeguard2>5):
        print("Safeguards failed. Exiting now")
        print("send_dict: "+str(send_dict))
        print("res_dict: "+str(res_dict))
        return

    global campaign_id

    # LET'S BEGIN

    #Begin campaign here
    response = [None]
    
    make_request(response,"startCampaign","&cards="+deck_send+"&reserves="+res_send+"&campaign_id="+str(campaign_id)+"&commander_id="+str(findnum(commander))+"&difficulty="+str(clevel))
    response = response[0]
    if bool(response['result'])==False:
        print("Response returned false. Error message:")
        print(response['result_message'])
        raise ValueError
        
    #battle_id = response['battle_data']['battle_id']
    randsleep(1,3)

    global cheating

    for lvl in range(1,8): # Assuming 7 levels to the campaign
        if lvl == 7:
            campaign = 'gw-defense' # Because don't care about card deaths
        else:
            campaign = 'campaign'
        enemy = cname+str(clevel)+'0'+str(lvl) #+'-'+str(clevel)
        command = 'tuo "'+current+'" "'+enemy+'" '+campaign+' ordered vip "'+vipstr+'" -o="'+temp+'" -c climb '+str(iterations)
        print("Command: "+command)
        a = cli(command,'Optimized Deck:')
        a.seek(-1)
        b = a.read().strip().split(': ')
        a.close()
        print("Working round sim. Level: "+str(lvl)+" Cards: "+b[2])
        print("Win rate: "+str(float(b[1].split(') ')[1]))+"\n\n")
        cards = b[2].strip().split(', ',1)[1] # Cardnames minus commander
        ordered_cards = cards.split(', ') # Cards, in order
        newcards = []

        for i in range(len(ordered_cards)):
            element = ordered_cards[i].split(' #')
            try:
                for j in range(int(element[1])):
                    #print(str(j))
                    newcards.append(element[0])
            except:
                newcards.append(element[0])

        ordered_cards = newcards
        print(str(ordered_cards))
        order_deck = []

        order_set = set(ordered_cards)
        print(str(order_set))
        order_dict = {}
        for element in order_set:
            order_dict[str(findnum(element))]=str(ordered_cards.count(element))
        send_dict = make_string(str(order_dict))
        
        for i in range(len(ordered_cards)):
            order_deck.append((int(findnum(ordered_cards[i])),ordered_cards[i]))

        response = [None]
        make_request(response,"setCampaignDeck","&cards="+send_dict+"&campaign_id="+str(campaign_id))
        response = response[0] # Don't really need it, so meh
        if bool(response['result'])==False:
            print("Response returned false. Error message:")
            print(response['result_message'])
            raise ValueError
        

        response = [None]
        make_request(response,"fightCampaignBattle","&campaign_id="+str(campaign_id))
        response = response[0]
        if bool(response['result'])==False:
            print("Response returned false. Error message:")
            print(response['result_message'])
            raise ValueError
        
        #battle_id = response['battle_data']['battle_id']
        randsleep(1,3)

        if response['user_data']['flags']['autopilot'] != u'0':
            make_request([0],'setUserFlag','&value=0&flag=autopilot')
            print("Set auto flag to 0")
            randsleep(1,3)
        deck = response['battle_data']['defend_deck'] # Not attack, since surge
        index = 104
        hand = [(deck['101'],101),(deck['102'],102),(deck['103'],103)]
        while bool(response['battle_data']['upkept']):

            if cheating: # Upkept just checked, see if new deaths have occurred
                deaths = []
                for item in response['battle_data']['turn']:
                    try:
                        dead_cards = response['battle_data']['turn'][item]['defend_kill']
                    except:
                        continue # Means it's an old turn or no deaths, so ignore it

                    for element in response['battle_data']['turn'][item]['defend_kill']:
                        deaths.append(response['battle_data']['card_map'][str(element)])
                if deaths: # Note, not only current deaths, but all deaths this fight
                    print("There have been card deaths:")
                    for element in deaths:
                        print(str(findcard(int(element))))
                    print("Resetting level now.")

                    # Fight battle, and restart campaign fight, having new values for deck,index,hand
                    global flag, brflag
                    result = True
                    yes = True
                    while yes:
                        result = choose_fight(target(),flag,brflag)
                        if result:
                            print("Kep, fought")
                            yes = False
                            continue
                        else:
                            print("Out of energy. Waiting 5 min")
                            time.sleep(5*60)

                    # Now time to fight new campaign battle
                    response = [None]
                    make_request(response,"fightCampaignBattle","&campaign_id="+str(campaign_id)) # Can keep everything same because sim
                    response = response[0]
                    if bool(response['result'])==False:
                        print("Response returned false. Error message:")
                        print(response['result_message'])
                        raise ValueError

                    randsleep(1,3)

                    if response['user_data']['flags']['autopilot'] != u'0':
                        make_request([0],'setUserFlag','&value=0&flag=autopilot')
                        print("Set auto flag to 0")
                        randsleep(1,3)
                    deck = response['battle_data']['defend_deck']
                    index = 104
                    hand = [(deck['101'],101),(deck['102'],102),(deck['103'],103)]

            
            move = -1
            for i in range(len(order_deck)):
                for j in range(len(hand)):
                    if (int(hand[j][0])>=int(order_deck[i][0])) and (int(hand[j][0])<=(int(order_deck[i][0])+5)):
                        print("Hand: "+str(hand))
                        move = hand.pop(j)
                        move = move[1]
                        print("Playing: "+order_deck[i][1])
                        
                        if ((index-100)<=len(deck)):
                            hand.append((deck[str(index)],index))
                            index+=1
                        break
                if move!=-1:
                    break
            if move==-1:
                for k in range(i,len(order_deck)):
                    move = hand.pop(j)
                    a = move[0]
                    move = move[1]
                    print("Deck goof")
                    print("Playing card: "+str(findcard(a)))
                    response = [None]
                    #make_fast_request(response,"playCard","&battle_id="+str(battle_id)+"&skip=0&card_uid="+str(move)+"&host_id="+player_id)
                    make_fast_request(response,"playCard","&battle_id=0&skip=0&card_uid="+str(move)+"&host_id=2") # TU Goofed
                    response = response[0]
                win = bool(int(response['battle_data']['winner']))
                if win:
                    print("Won campaign. Fix your deck.")
                else:
                    print("Lost campaign. Fix your deck.")
                    return
            else:
                response = [None]
                #make_fast_request(response,"playCard","&battle_id="+str(battle_id)+"&skip=0&card_uid="+str(move)+"&host_id="+player_id)
                make_fast_request(response,"playCard","&battle_id=0&skip=0&card_uid="+str(move)+"&host_id=2") # TU Goofed
                response = response[0]
        win = bool(int(response['battle_data']['winner']))
        if win:
            print("Won campaign fight")
            if lvl==7:
                print("Won campaign")
                for element in response['new_cards']:
                    print("New card: "+str(findcard(element)))
                return
        else:
            print("Lost campaign")
            return

        # Handles cardlosses here
        # Retrieve card deaths
        # death_dict

        deaths = []
        for item in response['current_campaigns'][str(campaign_id)]['cards']:
            element = response['current_campaigns'][str(campaign_id)]['cards'][item]
            card_id = str(element['card_id'])
            dead = int(element['num_lost'])
            if str(card_id) == str(findnum(commander)): # Catches the commander case
                continue
            old_death = int(death_dict[card_id])
            for i in range(dead-old_death):
                deaths.append(str(card_id))
            death_dict[card_id]=dead
        
        print("Card deaths: "+str(deaths))
        if deaths:
            inf = open(temp)
            current = []
            for line in inf:
                if line=='\n':
                    continue
                temps = line.strip().strip('[').split(']')
                temps0 = int(temps[0].strip(']'))
                try:
                    temps1 = int(temps[1].strip(' (').strip(')'))
                except:
                    temps1=1
                for lol in range(temps1):
                    current.append(temps0)
            #new_cards = []
            for element in deaths:
                i = current.index(int(element)) # element is a string, current has integers
                dead = current.pop(i)
                print("Removing "+str(dead)+": "+str(findcard(int(dead)))+" from cards")
            inf.close()

            temp=str(player_id)+' campaign inventory.txt'
            tempinv = open(temp,'w')

            setty = set(current)
            for element in setty:
                numstring=''
                if current.count(element)!=1:
                    numstring=' ('+str(current.count(element))+')'
                tempinv.write('['+str(element)+']'+numstring+'\n')
            tempinv.close() # Note, each filewrite overwrites file

            print("Current: "+str(current))

            for i in range(len(current)):
                current[i] = findcard(current[i])
            
            current = ', '.join(current) # From earlier: current = str(commander)+', '+', '.join(final_list)

        print("New set of cards: "+str(current))

    #------
    # Set cards to 
    # Preserve deck required for first mission in a variable
    # Create new text file for cards brought with
    # Run climb on first deck given that VIPs have now been set, execute first mission
    # Update deck by running campaign climb on next mission. Remember to account for card deaths
    # Repeat until last mission
    # Run guildwar climb on last mission

    # Sample climb:
    # 'tuo "Start" "Tartarus Vanguard 3-1" campaign random vip "Stoneheart, Ezamit Serene" -o="Main ownedcards.txt" fund 200 climb 10000'

def mission_energy(mission):
    cwd = os.getcwd()+'\\data\\'
    tree = ET.parse(cwd+'missions.xml')
    root = tree.getroot()
    for child in root.findall('mission'):
        if int(child.find('id').text) == int(mission):
            a = int(child.find('energy').text)
            del root
            del tree
            return a
        else:
            continue
    print("Mission energy goofed")
    del root
    del tree
    return -1

def decktonum(deck):
    cwd = os.getcwd()+'\\data\\'
    tree = ET.parse(cwd+'cards.xml')
    root = tree.getroot()
    retlist = []
    for card in deck.split(", "):
        for child in root.findall('unit'):
            if child.find('name').text == card:
                retlist.append((int(child.find('id').text),card))
            else:
                continue
    print("Deck assembled")
    print(retlist)
    del root
    del tree
    return retlist

def battle():
    print("I have awoken")
    
    global flag, brflag
    global wins, losses,gold,brgain,fight_count,player_id, stamina_cap, mission_trigger
    thiswin = wins
    thisloss = losses
    thisgold = gold
    thisbr = brgain

    global cheating, climb_mission
    if (not cheating) or (climb_mission!=5):
        result = True
        while result:
            result = choose_fight(target(),flag,brflag)
        print("\nFinished dumping battles\n")

        if mission_trigger:
            climb()
    else: # Huehuehuehue
        print("Cheats On")
        if mission_trigger:
            climb()
        
        result = True
        while result:
            result = choose_fight(target(),flag,brflag)
        print("\nFinished dumping battles\n")

    try:
        randsleep(1,5)
        response = [None]
        make_request(response,"update","")
        randsleep(1,5)
        response = response[0]
        if bool(response['daily_bonus']):
            response = [None]
            make_request(response,"useDailyBonus","")
            response = response[0]
            card = int(response['bonus_result']['bonus']['card'])
            card = findcard(card)
            print("Acquired Daily Bonus: "+str(card))
    except:
        pass
    
    print("Wins: "+str(wins-thiswin)+" (Total: "+str(wins)+")")
    print("Losses: "+str(losses-thisloss)+" (Total: "+str(losses)+")")
    print("Gold: "+str(gold-thisgold)+" (Total: "+str(gold)+")")
    print("BR: "+str(brgain-thisbr)+" (Total: "+str(brgain)+")")
    stamina_sleep = stamina_cap*5*60
    if False:
        print("Hibernating")
        randsleep(5*60*60,8*60*60)
    else:
        #randsleep(stamina_sleep//2,int(stamina_sleep*1.35))
        print("Sleeping for: "+str(stamina_sleep)+" seconds")
        time.sleep(stamina_sleep)
        print('\n\n')

def hibernate():
    a = randint(1,18)
    return a == 1


if __name__ == '__main__': # ONLY EDIT FROM HERE DOWN
    global player_id,timestamp,pass_timestamp,kong_id
    player_id = "0123456" # user_id in json requests (please leave as string)
    timestamp = str(int(time.time()))
    pass_timestamp = "password=asdfasdfasdfasdfasdfasdfasdfasdf&user_id=0123456" # Password (follow template, don't input timestamp or hash here)
    kong_id = "kong_id=0123456&kong_token=asdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdf&kong_name=asdfasdfasdf" # Kong_id stuff (follow template)

    if player_id == "0123456":
        print("Please remember to input your TU credentials before running this script")
        raise ValueError

    # MISSION ENERGY USAGE:
    global mission_trigger, fifty_mission, climb_mission, start_mission, end_mission, default_mission, current_mission
    climb_mission = 0 # 5 is for campaigns, 4 is for events, 3 is to 50/50, 2 makes this speed missions, 1 makes this climb (max) missions, 0 makes it repeat default_mission
    default_mission = 106 # mission_id (see missions.xml) if climb_mission == 0
    fifty_mission = 2387 # If climb_mission == 3

    global decks, cname, clevel, campaign_id, cenergy # Edit this section here if doing things for campaign
    startdeck = "Start" # Deck defined in customdecks.txt to start climbing from for campaign
    cname = "Secr" # Name of campaign in customdecks.txt (assumes takes the form BrimX0Y where X=difficulty, Y=fight)
    campaign_id = '3' # If climb_mission == 5
    clevel = 2 # Level of campaign
    cenergy = 125 # Energy of campaign

    global cheating
    cheating = 1 # Don't do this if you play fair ;) (For campaigns only)

    global listy_final, coverride, listy_vip
    coverride = 1 # Force campaign to choose these cards instead if coverride=1
    # For listy_final, if you have any duplicates, don't use (X), rather input them multiple times (e.g. 2x Baughe would mean 'Baughe','Baughe'). Also, count to make sure this list length is <=15
    listy_final = ['The Tenacious', 'Dreamhaunter', 'Rancorous Eupnoi', 'Revolt Ranger', 'Jilted Baughe', 'Baughe', 'Baughe', 'Abhorrent Recluse', 'Dune Runner', 'Dune Runner', 'Extinguisher of Hope', 'Ezamit Tranq', 'Basilisk Unearthed-5', 'Astral Strutter']
    listy_vip = ['Dreamhaunter','Rancorous Eupnoi','Astral Strutter','Ezamit Tranq']
    
    mission_trigger=1 # Start/stop using mission energy (1,0)

    global eventflag
    eventflag = 0 # Leave this be

    # Event definition: Speed start_mission to end_mission (end_mission==boss)
    # 50/50 boss
    # Max other missions
    # Once completed, repeat default mission

    global order_flag,order_deck
    order_flag=0 # Order (Note that order is imposed for campaign, so if climb_mission==5 then don't worry about this flag)

    if order_flag:
        order_cards = "Astral Strutter, Ezamit Tranq, Sage Ascendant, Jilted Baughe, Baughe" # The order of the cards. If duplicates, write element repeatedly rather than using parenthesis notation
        order_deck = decktonum(order_cards)

    if climb_mission: # Change if climb_mission != 0 or 3
        start_mission = 101
        current_mission = start_mission # Leave this line be
        end_mission = 106 # Note that climb still works if start_mission == end_mission. It'll just max that single mission. Do this, say, if you're looking to complete a mutant

    if climb_mission == 4: # Ignore everything in this if statement
        eventflag = 1
        fifty_mission = end_mission
        climb_mission = 2

    if climb_mission == 5: # Ignore this also
        cwd = os.getcwd()+'\\data'
        f = open(cwd+'\\customdecks.txt')
        climb_flag = 0
        for line in f:
            a = line.strip().split(': ')
            if a[0] == startdeck:
                decks = a[1]
                climb_flag = 1
                break
        if not climb_flag:
            print("Couldn't find deck. Please add it to customdecks.txt")
            raise ValueError
        print("Found deck")
        global inv_name
        inv_name = inv_scrape()
        
    # BATTLE STAMINA USAGE
    global flag, brflag # For battles, change as necessary
    flag = 0 # Change this as necessary. 2 is BR aggressive (always hitting highest BR player), 0 is gold aggressive (always lowest BR player), 1 is in the middle (around your BR)
    brflag = -1 # Turns on or off refreshing after X battles, where -1 turns it off (Note, this function doesn't work, please keep this as it is)

    global stamina_cap
    stamina_cap = 19 # Change this to be your stamina cap (I do cap - 1 to prevent overflow. In the event of cheating during campaign, set this to much lower than your stamina cap)

    erroring = 0 # Ignore this
    
    while True: # Ignore everything here too. It just handles errors if the program bugs
        try:
            battle()
            erroring = 0
        except Exception as e:
            erroring += 1 # Each time the program bugs in one run through the entire code, erroring will increment.
            print("Erroring: "+str(erroring))
##            print("Fine, turning missions off")
##            mission_trigger=0
            #print(e)
            traceback.print_exc()
            time.sleep(erroring*10)
            if erroring >5: # If erroring bypasses a threshold value (5 in this case) then chances are you either lost your internet connection, are banned, or something else.
                print("I've errored 5 times in a row. Calling it quits for now")
                break # Either way, if the threshold is hit, it's worth shutting off the bot, and so this statement does it
            print("Trying again")
