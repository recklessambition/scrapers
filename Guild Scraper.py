import urllib2, StringIO, gzip, zlib, requests, json,time,os,sys
import xml.etree.ElementTree as ET
import subprocess
import hashlib
from urllib2 import quote
import traceback

# Note: Will have to be modified to utilize HTTPS protocols at some point

# Functions:
# sim_guild(): Scrapes all guildmember decks from TU, dumps them to a txt, reorders them against a custom enemy, and then outputs the reorder results (format: Username, Deck, Score) (is a macro for card_list() and topclimb() )
# card_list(): Scrapes decks
# topclimb(): Takes decks.txt and reorders them against custom enemy
# war_scrape(): Scrapes the damage each member did in guildwar
# guild_br(): Scrapes guildmember BR
# raid_scrape(): Scrapes guildmember raid damages
# inv_scrape(): Scrapes your inventory (leaves them as numbers)
# name_scrape(): Scrapes your inventory, converting into names (takes longer)
# sp_scrape(): Scrapes your inventory and filters out all level 1 cards
# set_deck(): Sets the cards in your active deck
# quick_deck(): Can set custom macros for set_deck() (saves typing everything out repeatedly)

def make_request(response,message,body):
    response[0] = None
    global player_id,pass_timestamp,kong_id,timestamp
    timestamp = str(int(time.time()))
    try:
        url = "http://mobile.tyrantonline.com/api.php?message="+message+"&user_id="+player_id+"&timestamp="+timestamp

        req = {"User-Agent":"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:35.0) Gecko/20100101 Firefox/35.0",
        "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
        "Accept-Language":"en-US,en;q=0.5",
        "Accept-Encoding":"gzip, deflate",
        "Connection":"keep-alive",
        "Content-Type":"application/x-www-form-urlencoded",
        "X-Unity-Version":"4.6.3f1"}
        seed = '@B3%Y6'
        m=hashlib.md5(seed+str(player_id)+str(timestamp)).hexdigest()
        body = pass_timestamp+"&timestamp="+timestamp+"&hash="+str(m)+"&message="+message+"&client_version=45&device_type=Intel(R)+Core(TM)+i5-4690K+CPU+%40+3.50GHz+(16328+MB)&os_version=Windows+7+Service+Pack+1+(6.1.7601)+64bit&platform=Web&"+kong_id+body

        response[0] = requests.post(url=url,data=body,headers=req)
        ans = json.loads(response[0].text, encoding=response[0].headers['content-encoding'])
        response[0].close()
        response[0] = ans
    except:
        print("hwat")
        return False

    return True

def cli(inst,seek):
    f = StringIO.StringIO()
    process = subprocess.Popen(inst, stdout=subprocess.PIPE)
    flag = 0
    for line in iter(process.stdout.readline, ''):
        #print(line)
        #sys.stdout.write(line)
        if line.find(seek)!=-1: #'Optimized Deck:'
            flag = 1
        if flag:
            print(line)
            f.write(line)
    return f

def topclimb(decks=''):
    if not decks:
        decks = str(raw_input("Please input decks.txt filename: "))
    print("Note, if you want to leave a command blank, just hit enter")
    enemy = str(raw_input("Please input the enemy: "))
    event = str(raw_input("Please input the event (pvp,gw,gw-defense,brawl,raid): "))
    tower = str(raw_input("Please enter your towers (semi-colon separated list to sim multiple tower combinations, i.e. Lightning Cannon, Lightning Cannon; Death Factory, Death Factory): "))
    enemyTower = str(raw_input("Please enter enemy towers (semi-colon separated list to sim multiple tower combinations, i.e. Lightning Cannon, Lightning Cannon; Death Factory, Death Factory): "))
    effect = str(raw_input("Please input the bge (comma-separated list to SIM multiple): "))
    ordered = str(raw_input("Do you want to sim ordered (O) or random (R)? "))
    iterations = int(raw_input("Please input the number of sim iterations (recommended: 1000): "))
    effects = effect.split(',')
    towers = tower.split(';')
    enemyTowers = enemyTower.split(';')
    if len(towers) > 1:
        if len(enemyTowers) > 1:
            for et in enemyTowers:
                for t in towers:
                    print("Simming " + t + " vs " + et)
                    filename = t.replace(' ', '_').replace(',', '') + "_vs_" + et.replace(' ', '_').replace(',', '') + '.xls'
                    reorder(enemy,event=event,tower=t.strip(),enemyTower=et.strip(),effect=effect,override=1,inn=decks,ordered=ordered,iterations=iterations,filename=filename)
        else:
            for t in towers:
                print("Simming tower combo: " + t)
                filename = t.replace(' ', '_').replace(',', '') + '.xls'
                reorder(enemy,event=event,tower=t.strip(),enemyTower=enemyTower,effect=effect,override=1,inn=decks,ordered=ordered,iterations=iterations,filename=filename)
    elif len(effects) > 1:
        for e in effects:
            print("Simming BGE: " + e)
            filename = e.replace(' ', '_') + ".xls"
            reorder(enemy,event=event,tower=tower,enemyTower=enemyTower,effect=e.strip(),override=1,inn=decks,ordered=ordered,iterations=iterations,filename=filename)
    else:
        reorder(enemy,event=event,tower=tower,enemyTower=enemyTower,effect=effect,override=1,inn=decks,ordered=ordered,iterations=iterations,filename="")

def reorder(enemy,event,tower,enemyTower,effect,override,inn,ordered,iterations,filename):
    if not override:
        flag = 0
        while not flag:
            inn = str(raw_input("Please input the name of the .txt to intake decks from."))
            try:
                inf = open(inn)
                flag = 1
            except:
                print("Unable to access file, please try again")
    else:
        inf = open(inn)
    flag = 0
    
    if not filename or filename == '':
        while not flag:
            out = str(raw_input("Please input the name for a .xls to output optimized decks to: "))
            try:
                outf = open(out,'w')
                flag = 1
            except:
                print("Unable to access file, please try again")
    else:
        while not flag:
            try:
                outf = open(filename,'w')
                flag = 1
            except:
                filename = str(raw_input("Unable to access file, please input your own filename: "))
    
    cwd = os.getcwd()+'\\data'
    
    if ordered is 'R' or ordered is 'r':
        ordered = 'random'
        operation = 'sim'
        outf.write('User\tScore')
    else:
        ordered = 'ordered'
        operation = 'reorder'
        outf.write('User\tDeck\tScore')

    for line in inf:
        if line=='\n':
            continue
        new = line.strip().split(':')
        user = new[0]
        olddeck = new[1]

        if tower:
            towers = ' yf "'+tower+'"'
        else:
            towers = ''
        if enemyTower:
            towers += ' ef "'+enemyTower+'"'
            
        effect_out = ''
            
        if effect:
            if '+' in effect:
                effects = effect.split('+')
                for e in effects:
                    effect_out += ' -e "'+e.strip()+'"'
            else:
                effect_out = ' -e "'+effect+'"'

        iterations = str(iterations)
        print("\n\n")
        command = 'tuo "'+olddeck+'" "'+enemy+'" '+event+' '+ordered+''+towers+' ' + effect_out + ' ' + operation + ' ' +iterations
        print("Command: " + command)
        if operation is 'reorder':
            a = cli(command,'Optimized Deck:')
            a.seek(-1)
            b = a.read().strip('\n').strip('\r').split(': ')
            c = b[2]
            outline = '\n'+user+'\t'+b[3]+'\t'+c
            outf.write(outline)
        else:
            a = cli(command,'win%:')
            a.seek(-1)
            b = a.read().split(': ')
            c = b[1][0]
            outline = '\n'+user+'\t'+c
            outf.write(outline)
        print("Completed sim for "+user)
    inf.close()
    outf.close()
    print("Completed")
    print("Note: You can trust the .xls file")

def sim_guild():
    decks = card_list()
    topclimb(decks)

def card_list(): # Assembles a TUO text file of guild decks
    a = False
    while not a:
        out = str(raw_input("Please input the name of the .txt file to output to: "))
        try:
            outf = open(out,'w')
            a = True
        except:
            print("Unable to open file. Please try again")
    a = False
    while not a:
        out2 = str(raw_input("Do you want to retrieve Attack (A) or Defense (D) decks? "))
        if out2 is 'A' or out2 is 'a' or out2 is 'D' or out2 is 'd':
            a = True
    response = [None]
    make_request(response,'updateFaction','')
    response = response[0]
    members = list(response['faction']['members'].keys())
    for i in members:
        member_id = str(i)
        response = [None]
        make_request(response,"getProfileData",'&target_user_id='+member_id)
        response = response[0]
        string = response['player_info']['name'] + ': '
        if out2 is 'A' or out2 is 'a':
            string += '['+response['player_info']['deck']['commander_id']+']'
            cards = list(response['player_info']['deck']['cards'])
        else:
            string += '['+response['player_info']['defense_deck']['commander_id']+']'
            cards = list(response['player_info']['defense_deck']['cards'])
        for element in cards:
            string += ', ['+str(element)+']'
        string += '\n'
        outf.write(string)
    outf.close()
    print("Completed")
    return out

def war_scrape(): # Assembles a TUO text file of guild decks
    a = False
    while not a:
        out = str(raw_input("Please input the name of the .csv file to output to: "))
        try:
            outf = open(out,'w')
            a = True
        except:
            print("Unable to open file. Please try again")
    outf.write('Member,War Points\n')
    response = [None]
    make_request(response,'updateFaction','')
    response = response[0]
    members = list(response['faction']['members'].keys())
    for i in members:
        member_id = str(i)
        response = [None]
        make_request(response,"getProfileData",'&target_user_id='+member_id)
        response = response[0]
        string = response['player_info']['name'] + ',' + response['player_info']['faction_war_points']
        string += '\n'
        outf.write(string)
    outf.close()
    print("Completed")
    return out

def guild_br():
    a = False
    while not a:
        out = str(raw_input("Please input the name of the .csv file to output to: "))
        try:
            outf = open(out,'w')
            a = True
        except:
            print("Unable to open file. Please try again")
    response = [None]
    make_request(response,'updateFaction','')
    response = response[0]
    outf.write('Member, BR\n')
    listy = response['faction']['members'].items()
    for i in listy:
        string = str(i[1]['name'])+','
        string += str(i[1]['rating'])
        string += '\n'
        outf.write(string)
    outf.close()
    print("Completed")

def raid_scrape():
    a = False
    while not a:
        out = str(raw_input("Please input the name of the .csv file to output to: "))
        try:
            outf = open(out,'w')
            a = True
        except:
            print("Unable to open file. Please try again")
    response = [None]
    make_request(response,'getRaidInfo','')
    response = response[0]
    try:
        listy = response['raid_info'][str(response['raid_info'].keys()[0])]['members'].items()
    except:
        print("There is no current raid")
        outf.close()
        return
    outf.write('Member, damage\n')
    for i in listy:
        string = str(i[1]['member_name'])+','
        string += str(i[1]['damage'])
        string += '\n'
        outf.write(string)
    outf.close()
    print("Completed")
    
def conquest_scrape():
    a = False
    while not a:
        out = str(raw_input("Please input the name of the .csv file to output to: "))
        try:
            outf = open(out,'w')
            a = True
        except:
            print("Unable to open file. Please try again")
    response = [None]
    make_request(response,'getGuildInfluenceLeaderboard','')
    response = response[0]
    try:
        listy = response['conquest_influence_leaderboard']['data']
    except:
        print sys.exc_info()[0]
        print("There is no current raid")
        outf.close()
        return
    outf.write('Member, damage\n')
    for i in listy[1:]:
        string = str(i['name'])+','
        string += str(i['influence'])
        string += '\n'
        outf.write(string)
    outf.close()
    print("Completed")

def inv_scrape():
    a = False
    while not a:
        out = str(raw_input("Please input the name of the .txt file to output to: "))
        try:
            outf = open(out,'w')
            a = True
        except:
            print("Unable to open file. Please try again")
    response = [None]
    make_request(response,'update','')
    response = response[0]
    try:
        cards = response['user_cards']
    except:
        print("Unable to parse cards?")
        outf.close()
        return
    keys = cards.keys()
    for i in range(len(cards)):
        num = int(cards[keys[i]]['num_owned'])
        if num:
            string = str('[')+str(keys[i])+str(']')
            if num>1:
                string += ' ('+str(num)+')'
            string += '\n'
            outf.write(string)
    outf.close()
    print("Completed")

def findcard(card, root):
    upgrades = {'0': 0, '1':3,'2':4,'3':6,'4':6,'5':6}
    for child in root.findall('unit'):
        rarity = child.find('rarity').text
        upgrade = upgrades[rarity]
        if (int(card) >= int(child.find('id').text)) and (int(card) <= (int(child.find('id').text)+upgrade-1)):
            #print("Here")
            level = int(card)-int(child.find('id').text)+1
            leveltext = ''
            if level != int(upgrade):
                leveltext = '-'+str(level)
            name = str(child.find('name').text)+leveltext
            faction = int(child.find('type').text)
            return (name, faction)
    print("Couldn't find card with id: "+str(card))
    raise ValueError

def falsecard(card):
    try:
        cwd = os.getcwd()+'\\data\\'
        tree = ET.parse(cwd+'cards.xml')
        root = tree.getroot()
    except:
        print("Please put this script in the TUO main folder")
        raise ValueError
    upgrades = {'1':3,'2':4,'3':6,'4':6,'5':6}
    for child in root.findall('unit'):
        rarity = child.find('rarity').text
        upgrade = upgrades[rarity]
        if (int(card) >= int(child.find('id').text)) and (int(card) <= (int(child.find('id').text)+upgrade-1)):
            #print("Here")
            level = int(card)-int(child.find('id').text)+1
            leveltext = ''
            if level != int(upgrade):
                return False
            return str(child.find('name').text)
    print("Couldn't find card with id: "+str(card))
    raise ValueError

def name_scrape(sortByFaction):
    a = False
    while not a:
        out = str(raw_input("Please input the name of the .txt file to output to: "))
        try:
            outf = open(out,'w')
            a = True
        except:
            print("Unable to open file. Please try again")
    response = [None]
    make_request(response,'update','')
    response = response[0]
    try:
        cards = response['user_cards']
    except:
        print("Unable to parse cards?")
        outf.close()
        return

    try:
        cwd = os.getcwd()+'\\data\\'
        tree = ET.parse(cwd+'cards.xml')
        root = tree.getroot()
        for i in range(1,12):
            tempTree = ET.parse(cwd+'cards_section_'+str(i)+'.xml')
            units = tempTree.iter('unit')
            for unit in units:
                root.append(unit)
        outcards = open("cards.xml", 'w')
        rough_string = ET.tostring(root, 'utf-8')
        outcards.write(rough_string)
        outcards.close()
        #print(rough_string)
    except IOError as e:
        print "I/O error({0}): {1}".format(e.errno, e.strerror)
        raise ValueError
    except NameError as e:
        print e
        print "Name error{0}".format(e.strerror)
        raise ValueError
    except:
        print sys.exc_info()[0]
        print("Please put this script in the TUO main folder")
        raise ValueError

    keys = cards.keys()
    cardTuples = []
    for i in range(len(cards)):
        num = int(cards[keys[i]]['num_owned'])
        if num:
            card = findcard(str(keys[i]), root)
            cardTuple = (card[0], card[1], num)
            cardTuples.append(cardTuple);
    if sortByFaction:
        cardTuples = sorted(cardTuples, key=lambda tup: tup[1])
    currentFaction = 0;
    factions = {'1': 'Imperial', '2': 'Raider', '3': 'Bloodthirsty', '4': 'Xeno', '5': 'Righteous', '6': 'Progenitor'}
    for cardTuple in cardTuples:
        name = cardTuple[0]
        faction = cardTuple[1]
        num = cardTuple[2]
        if num > 1:
            name += ' (' + str(num) + ')'
        name += '\n'
        if sortByFaction and faction != currentFaction:
            currentFaction = faction;
            outf.write('\n### ' + factions[str(faction)] + '\n')
        outf.write(name)
    outf.close()
    print("Completed")

def sp_scrape():
    a = False
    while not a:
        out = str(raw_input("Please input the name of the .txt file to output to: "))
        try:
            outf = open(out,'w')
            a = True
        except:
            print("Unable to open file. Please try again")
    response = [None]
    make_request(response,'update','')
    response = response[0]
    try:
        cards = response['user_cards']
    except:
        print("Unable to parse cards?")
        outf.close()
        return
    keys = cards.keys()
    for i in range(len(cards)):
        num = int(cards[keys[i]]['num_owned'])
        if num:
            string = falsecard(str(keys[i]))
            if not string:
                continue
            if num>1:
                string += ' ('+str(num)+')'
            string += '\n'
            outf.write(string)
    outf.close()
    print("Completed")

def make_string(string):
    string = str(string).replace(" ","").replace("'",'"')
    return quote(string)

def findnum(card):
    try:
        cwd = os.getcwd()+'\\data\\'
        tree = ET.parse(cwd+'cards.xml')
        root = tree.getroot()
    except:
        print("Please put this script in the TUO main folder")
        raise ValueError
    for child in root.findall('unit'):
        xmlcard = str(child.find('name').text)
        actualcard = card.split('-')
        if xmlcard == str(actualcard[0]):
            try:
                retcard = int(child.find('id').text)-1+int(actualcard[1])
            except:
                retcard = int(child.find('id').text)-1+6
            return retcard
    print("Could not find card with name: "+str(card))
    raise ValueError

def set_deck(deck=''):
    if not deck:
        deck = str(raw_input("Please input a comma-separated list for your deck (beginning with commander): "))
    try:
        decks = deck.strip().split(',')
        commander = decks.pop(0).strip()
        for i in range(len(decks)):
            decks[i] = decks[i].strip()
        setty = set(decks)
        newdeck={}
        for element in setty:
            newdeck[str(findnum(element))]=str(decks.count(element))
        to_send = make_string(str(newdeck))
        commander_id = str(findnum(commander))
    ##    print(to_send)
    ##    print(commander_id)
    except:
        print("Something was invalid, please check spelling.")
        traceback.print_exc()
        return
    
    response = [None]
    make_request(response,'setDeckCards','&deck_id=1&cards='+to_send+'&commander_id='+commander_id+'&activeYN=1')
    response = response[0]
    print("Completed")

def quick_deck():
    inp = int(raw_input("0:Offense,1:Defense "))
    if inp==0:
        print("Setting deck to: Nexor, Chainslicer, Eupnoi, Ezamit Serene, Crushing Anvil, Baughe, Bane-5, Dreamreaper")
        set_deck('Nexor, Chainslicer, Eupnoi, Ezamit Serene, Crushing Anvil, Baughe, Bane-5, Dreamreaper')
    elif inp==1:
        print("Setting deck to: Barracus, Dreamhaunter, Abhorrent Recluse, Revolt Ranger, Savant Ascendant, Ezamit Tranq, Dune Runner (2), Basilisk Unearthed, Rancorous Eupnoi")
        set_deck('Barracus, Dreamhaunter, Abhorrent Recluse, Revolt Ranger, Savant Ascendant, Ezamit Tranq, Dune Runner, Dune Runner, Basilisk Unearthed, Rancorous Eupnoi')

def init_credentials(): # Edit the contents of this function
    global player_id,timestamp,pass_timestamp,kong_id
    player_id = "0123456" # user_id in json requests (please leave as string)
    timestamp = str(int(time.time()))
    pass_timestamp = "password=asdfasdfasdfasdfasdfasdfasdfasdf&user_id=0123456" # Password (follow template, don't input timestamp or hash here)
    kong_id = "kong_id=0123456&kong_token=asdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdf&kong_name=asdfasdfasdf" # Kong_id stuff (follow template)

    if player_id == "0123456":
        print("Please remember to input your TU credentials before running this script")
        raise ValueError

if __name__ == "__main__":
    init_credentials()
    # guild_br() fetches all members' BR, puts in csv
    # card_list() fetches all members' cards, puts in TUO compatible deck .txt file
    #name_scrape(False)
    #war_scrape()
    #sim_guild()
    conquest_scrape()
