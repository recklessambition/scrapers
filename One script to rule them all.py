import os, time, random, sys
import xml.etree.ElementTree as ET
import subprocess
import StringIO

# Functions:
# war() - Generates inventory text files and a decks file based on a .csv (see template war.csv in the same folder)
# primegauntlet() - Inputs a customdecks.txt gauntlet, and outputs the gauntlet decks primed for a specific bge (useful for brawls or guildwars)
# gauntlet() - Generates guild gauntlet
# raiddeck() - Generates random raid deck
# run() - Gets decks and inventories from .csv (using war() ), then runs climbs on all of them vs custom enemy

##def cli(inst):
##    listy = inst.strip().split(' ')
##    try:
##        a = subprocess.check_output(listy)
##        return a
##    except subprocess.CalledProcessError, e:
##        print(e.output)

def cli(inst,seek):
    f = StringIO.StringIO()
    process = subprocess.Popen(inst, stdout=subprocess.PIPE)
    flag = 0
    for line in iter(process.stdout.readline, ''):
        #sys.stdout.write(line)
        if line.find(seek)!=-1: #'Optimized Deck:'
            flag = 1
        if flag:
            f.write(line)
            #sys.stdout.write(line)
    return f

def primegauntlet():
    gauntlet = str(raw_input("Please enter the name of the gauntlet: "))
    cwd = os.getcwd()+'\\data\\'
    f=open(cwd+'customdecks.txt')

    flag = 0
    for line in f:
        if line.split(':')[0]==gauntlet:
            s = line.split(': ')[1].strip()
            flag = 1
            break
    if not flag:
        print("Couldn't find gauntlet. Try again")
    decks=[]
    s=s.strip('/^').strip('$/')
    if ('(' in s) and s.count('(')>1:
        g = s.strip(')').strip('(').split('(')
        nums = g[1].split('|')
        gtl = g[0]
        f.seek(0)
        for line in f:
            new = line.strip().split(': ')
            for element in nums:
                temp = gtl+element
                if (temp==new[0]):
                    decks.append(new[1])
                    break
    else:
        g = s.strip('\\d').strip(')').strip('(').split('|')
        f.seek(0)
        for line in f:
            new = line.strip().split(': ')
            for element in g:
                if (element in new[0]) and (element != new[0]):
                    decks.append(new[1])
                    break
    print("Decks: "+str(decks))

    iterations = str(raw_input('Number of iterations per deck? '))
    event = str(raw_input("Please input the event (pvp,gw,gw-defense,brawl,raid): "))
    effect=str(raw_input("Please input battleground effect (e.g. Enhance All Armor 1): "))
    sim='climb'
    if not effect:
        print("Did not put an effect down.")
        return
    effects='-e "'+effect+'"'

    newdecks=[]

    for deck in decks:
        command = 'tuo "'+deck+'" "'+gauntlet+'" '+event+' '+'random'+' '+effects+' '+sim+' '+iterations
        #print(command)
        a = cli(command,'Optimized Deck:')
        a.seek(-1)
        b = a.read().strip('\n').strip('\r').split(': ')[2]
        newdecks.append(b)

    print("Finished run on decks")

    counter=0
    f.close()
    f=open(cwd+'customdecks.txt','a')
    f.write('\n')

    for i in range(len(newdecks)):
        string = 'GTP'+str(counter).zfill(2)+': '+newdecks[i]+'\n'
        f.write(string)
        counter+=1
    
    b = "GTP: /^GTP\\d\\d$/\n"
    #print(b)
    f.write(b)
    f.close()
    print("Completed")

def run():
    decks = war()
    flag = int(raw_input("Do you want to run a climb on decks? (1-yes, 0-no)"))
    if flag:
        topclimb(decks)

def topclimb(decks=''):
    if not decks:
        decks = str(raw_input("Please input decks.txt filename: "))
    print("Note, if you want to leave a command blank, just hit enter")
    enemy = str(raw_input("Please input the enemy: "))
    event = str(raw_input("Please input the event (pvp,gw,gw-defense,brawl,raid): "))
    tower = str(raw_input("Please enter towers: "))
    effect = str(raw_input("Please input the bge: "))
    order = int(raw_input("Ordered? (1-yes, 0-no): "))
    iterations = int(raw_input("Please input the number of sim iterations (recommended: 1000): "))
    climb(enemy,event=event,tower=tower,effect=effect,order=order,override=1,inn=decks,iterations=iterations)

def climb(enemy,event='pvp',tower='',effect='',order=1,override=0,inn=0,iterations=1000):
    if not override:
        flag = 0
        while not flag:
            inn = str(raw_input("Please input the name of the .csv to intake decks from."))
            try:
                inf = open(inn)
                flag = 1
            except:
                print("Unable to access file, please try again")
    else:
        inf = open(inn)
    flag = 0
    while not flag:
        out = str(raw_input("Please input the name for a .xls to output optimized decks to: "))
        try:
            outf = open(out,'w')
            flag = 1
        except:
            print("Unable to access file, please try again")

    cwd = os.getcwd()+'\\data'
    outf.write('User\tDeck\tWinrate')

    for line in inf:
        if line=='\n':
            continue
        new = line.strip().split(':')
        user = new[0]
        olddeck = new[1]
        if order:
            ordered = 'ordered'
        else:
            ordered = 'random'

        if tower:
            towers = 'yf "'+tower+'"'

        iterations = str(iterations)
        command = 'tuo "'+olddeck+'" "'+enemy+'" '+event+' '+ordered+' '+towers+' climb '+iterations+' -o="'+str(user)+'.txt"'
        a = cli(command,'Optimized Deck:')
        a.seek(-1)
        b = a.read().strip('\n').strip('\r').split(':')
        c = b[1].split(') ')[1]
        outline = '\n'+user+'\t'+b[2]+'\t'+c
        outf.write(outline)
        print("Completed sim for "+user)
    inf.close()
    outf.close()
    print("Completed")
    print("Note: You can trust the .xls file")

def war():
    val2 = False
    while not val2:
        inp = raw_input("Please input the spreadsheet name: ")
        out = raw_input("Please input name of output deck file: ")

        try:
            f = open(inp)
            val2 = True
        except:
            print("Could not access file "+inp)
            val2 = False
            continue
        try:
            o = open(out,'w')
            val2 = True
        except:
            print("Could not access file "+out+". Does it already exist?")
            val2 = False
            continue

    while True:
        line = f.readline()
        if (line == '\n'):
            continue
        if (line == ''):
            break
        a = line.strip().strip(',').split(',',1)
        try:
            if a[1]:
                pass
        except:
            f.readline()
            continue
        b = line.strip().strip(',').split(',')
        inpu = open(a[0] + '.txt','w')
        new = f.readline().strip().split(',')[1:]
        for i in new:
            inpu.write(i+'\n')
        for i in b[1:]:
            inpu.write(i+'\n')
        inpu.close()
        string = a[0] + ": " + a[1] + '\n'
        #print(string)
        o.write(string)
    f.close()
    o.close()
    print("Completed")
    return out

def gauntlet():
    val2 = False
    while not val2:
        inp = str(raw_input("Please input the spreadsheet name: "))
        out = str(raw_input("Please input name of output file: "))

        try:
            f = open(inp)
            val2 = True
        except:
            print("Could not access file "+inp)
            val2 = False
            continue
        try:
            o = open(out,'w')
            val2 = True
        except:
            print("Could not access file "+out+". Does it already exist?")
            val2 = False
            continue

    start = str(raw_input("Please input name for gauntlet: "))

    num = 0
    for line in f:
        if (line == '\n'):
            continue
        a = line.strip().strip(',').split(',',1)
        string = start + str(num).zfill(2) + ": " + a[1] + '\n'
        #print(string)
        o.write(string)
        num+=1

    b = start + ": /^"+start+"\d\\d$/\n"
    #print(b)
    o.write(b)
    f.close()
    o.close()

def downconvert(card,level):
    newcard = int(card)-1+int(level)
    return '['+str(newcard)+']'

def raiddeck():
    a = False
    try:
        tree = ET.parse('raids.xml')
        root = tree.getroot()
    except:
        print("Please put this script in the TUO data folder")
        return
    while not a:
        commander = str(raw_input("Please input the raid commander: "))
        for child in root.findall('raid'):
            if child.find('name').text == commander:
                a = True
                level = int(raw_input("Please input the raid level (e.g. 11): "))
                commander = int(child.find('commander').text)
                fixed = []
                try:
                    for element in child.find('deck').find('always_include'):
                        fixed.append(int(element.text))
                except:
                    pass
                pool = []
                try:
                    for element in child.find('deck').find('card_pool'):
                        pool.append(int(element.text))
                    num_random = int(child.find('deck').find('card_pool').get('amount'))
                except:
                    num_random = 0
                
                del root
                del tree

                if level > 26:
                    print("Level is higher than 26, 26 used instead.")
                    level=26
                if level < 1:
                    print("Level is lower than 1, 1 used instead.")
                    level=1
                levels = [0]
                for i in range(1,26):
                    levels.append(i*4)
                upgrades = levels[level-1]
                upgrades = int(upgrades)
                deck = fixed[:]
                for i in range(num_random):
                    j = random.randint(0,len(pool)-1)
                    deck.append(pool.pop(j))
                random.shuffle(deck)
                if level==26:
                    string = downconvert(int(commander),10)
                    for element in deck:
                        string+=','+downconvert(int(element),10)
                    print(string)
                    return
                newdeck = []
                newdeck.append([commander,1])
                for element in deck:
                    newdeck.append([element,1])
                
                val = False
                while not val:
                    for j in range(len(newdeck)):
                        if upgrades<=0:
                            val = True
                        else:
                            i = random.randint(0,min(upgrades,10-newdeck[j][1]))
                            upgrades -= i
                            newdeck[j][1] += i
                ndeck = []
                for element in newdeck:
                    ndeck.append(downconvert(int(element[0]),int(element[1])))
                a = ','.join(ndeck)
                print(a)

            else:
                continue
        if not a:
            print("Raid commander not found. Please try again")

